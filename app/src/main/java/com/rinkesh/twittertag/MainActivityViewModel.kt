package com.rinkesh.twittertag

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.rinkesh.twittertag.entity.Tweet
import com.rinkesh.twittertag.network.NetworkLibrary
import java.util.*
import kotlin.concurrent.scheduleAtFixedRate


/**
 * Created by Rinkesh on 2019-06-25.
 */
class MainActivityViewModel : ViewModel() {
    var isLoading = MutableLiveData<Boolean>()
    var searchedTag = ""
    var isNewSearch = false
    var nextQuery = MutableLiveData<String>()
    private var liveTweetList = MutableLiveData<ArrayList<Tweet>>()
    var tweetList = ArrayList<Tweet>()
    private val timer = Timer()
    private lateinit var networkCall: NetworkLibrary.MyGetHttpRequestTask

    fun findTweetList(): MutableLiveData<ArrayList<Tweet>> {
        return liveTweetList
    }

    fun getAPICallStatus(): MutableLiveData<Boolean> {
        return isLoading
    }

    fun updateTimer() {
        timer.scheduleAtFixedRate(0, 10_000) {
            loadTweet(true)
        }
    }


    fun loadTweet(isFresh: Boolean) {
        if (searchedTag != "") {
            isLoading.postValue(true)
            val uiSearchUrl = getSearchQuery(isFresh)
            networkCall = NetworkLibrary.MyGetHttpRequestTask(liveTweetList, nextQuery)
            networkCall.execute(uiSearchUrl)
        }
    }

    private fun getSearchQuery(isFresh: Boolean): String {
        return if (isFresh || nextQuery.value == null) {
            "?q=%23$searchedTag&count=20&f=tweets"
        } else {
            nextQuery.value!!
        }

    }

    override fun onCleared() {
        super.onCleared()
        networkCall.cancel(true)
        timer.cancel()
    }
}
