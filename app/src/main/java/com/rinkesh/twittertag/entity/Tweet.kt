package com.rinkesh.twittertag.entity

/**
 * Created by Rinkesh on 2019-06-25.
 */
data class Tweet(val id:String,val name: String,val userName:String,val date: String,val tweetText: String)