package com.rinkesh.twittertag.utils

import android.support.v7.util.DiffUtil
import com.rinkesh.twittertag.entity.Tweet

/**
 * Created by Rinkesh on 2019-06-26.
 */
class DiffUtilsTweet(
    private val newData: ArrayList<Tweet>, private val oldData: ArrayList<Tweet>
) : DiffUtil.Callback() {
    override fun areItemsTheSame(p0: Int, p1: Int): Boolean {
        return true
    }

    override fun getOldListSize(): Int {
        return oldData.size
    }

    override fun getNewListSize(): Int {
        return newData.size
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldData[oldItemPosition].id == newData[newItemPosition].id
    }
}