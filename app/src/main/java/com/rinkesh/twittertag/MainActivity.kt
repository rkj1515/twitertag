package com.rinkesh.twittertag

import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.view.MenuItemCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.*
import android.util.Log
import android.view.Menu
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import kotlin.collections.ArrayList


class MainActivity : AppCompatActivity(), LifecycleObserver {
    lateinit var viewModel: MainActivityViewModel
    private lateinit var tweetAdaptor: TagViewAdaptor
    private val timer = Timer()
    val PAGE_SIZE = 8
    lateinit var layoutManager: LinearLayoutManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewModel = ViewModelProviders.of(this).get(MainActivityViewModel::class.java)
        setupRecyclerView()
        setupProgressBar()
        viewModel.updateTimer()
    }

    private fun setupProgressBar() {
        viewModel.getAPICallStatus().observe(this, Observer {
            if (it!!) {
                progressbar.visibility = View.VISIBLE
            } else {
                progressbar.visibility = View.GONE
            }
        })

    }


    override fun onDestroy() {
        super.onDestroy()
        timer.cancel()
    }

    private val recyclerViewOnScrollListener = object : RecyclerView.OnScrollListener() {

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            val visibleItemCount = layoutManager.childCount
            val totalItemCount = layoutManager.itemCount
            val firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition()

            if (!viewModel.isLoading.value!!) {
                if (visibleItemCount + firstVisibleItemPosition >= totalItemCount
                    && firstVisibleItemPosition >= 0
                    && totalItemCount >= PAGE_SIZE
                ) {
                    Log.d("MAIN_ACTIVITY", "LOAD MORE TWEET")
                    viewModel.isNewSearch = false
                    viewModel.loadTweet(false)
                }
            }
        }
    }

    private fun setupRecyclerView() {
        // Creates a vertical Layout Manager
        layoutManager = LinearLayoutManager(this)
        tag_list.layoutManager = layoutManager
        tweetAdaptor = TagViewAdaptor(ArrayList())
        tag_list.itemAnimator = DefaultItemAnimator()
        tag_list.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

        tag_list.addOnScrollListener(recyclerViewOnScrollListener)
        tag_list.adapter = tweetAdaptor
        viewModel.findTweetList().observe(this, Observer {
            viewModel.isLoading.value = false
            tweetAdaptor.onNewData(it!!, viewModel.isNewSearch)
        })

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu, menu)
        val searchViewItem = menu.findItem(R.id.search)
        val searchViewAndroidActionBar = MenuItemCompat.getActionView(searchViewItem) as SearchView
        searchViewAndroidActionBar.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                searchViewAndroidActionBar.clearFocus()
                return true
            }

            override fun onQueryTextChange(newText: String): Boolean {
                if (newText.length > 2) {
                    viewModel.loadTweet(true)
                    viewModel.searchedTag = newText
                    viewModel.isNewSearch = true
                    viewModel.nextQuery.value = null
                    viewModel.findTweetList()
                    viewModel.tweetList.clear()
                }
                return false
            }
        })
        return super.onCreateOptionsMenu(menu)
    }
}
