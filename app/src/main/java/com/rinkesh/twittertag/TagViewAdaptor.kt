package com.rinkesh.twittertag

import android.databinding.DataBindingUtil
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.rinkesh.twittertag.databinding.TagListItemBinding
import com.rinkesh.twittertag.entity.Tweet
import com.rinkesh.twittertag.utils.DiffUtilsTweet


/**
 * Created by Rinkesh on 2019-06-25.
 */

class TagViewAdaptor(val data: ArrayList<Tweet>) : RecyclerView.Adapter<TagViewAdaptor.TagHolder>() {


    override fun getItemCount(): Int {
        return data.size
    }

    fun onNewData(newData: ArrayList<Tweet>, isNewSearch: Boolean) {
        val diffResult = DiffUtil.calculateDiff(DiffUtilsTweet(newData, data))
        diffResult.dispatchUpdatesTo(this)
        if (isNewSearch) {
            this.data.clear()
        }
        this.data.addAll(newData)
    }


    // Inflates the item views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TagHolder {

        val binding = DataBindingUtil.inflate<TagListItemBinding>(
            LayoutInflater.from(parent.context), R.layout.tag_list_item, parent, false
        )
        return TagHolder(binding);
    }

    override fun onBindViewHolder(holder: TagHolder, index: Int) {
        holder.bindItems(data[index])
    }

    class TagHolder(private val itemViewBinding: TagListItemBinding) : RecyclerView.ViewHolder(itemViewBinding.root) {

        fun bindItems(tweet: Tweet) {
            itemViewBinding.tweet = tweet;
            itemViewBinding.executePendingBindings()

        }

    }
}