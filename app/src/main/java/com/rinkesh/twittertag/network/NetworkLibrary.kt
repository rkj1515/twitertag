package com.rinkesh.twittertag.network


import android.arch.lifecycle.MutableLiveData
import android.os.AsyncTask
import android.util.Log
import com.rinkesh.twittertag.entity.Tweet
import org.json.JSONObject
import java.io.BufferedOutputStream
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by Rinkesh on 2019-06-25.
 */
class NetworkLibrary {
    companion object {
        const val TWITTER_OATH = "https://api.twitter.com/oauth2/token"
        const val TWITTER_URL = "https://api.twitter.com/1.1/search/tweets.json"
    }

    //For access Token Generation Code
    class MyPOSTHttpRequestTask : AsyncTask<String, Int, String>() {

        override fun doInBackground(vararg params: String): String? {
            val url = URL(TWITTER_OATH)
            val httpURLConnection = url.openConnection() as HttpURLConnection
            httpURLConnection.requestMethod = "POST"
//            encode value of Secret key and Secret Client
            httpURLConnection.setRequestProperty(
                "Authorization",
                "Basic NjViaWtyMFNCTmFHeDExOThCQnZGVGR6QTpBeW1Pd3NHM2c2dkhleGJMbUY5NlRpSnlwNDdwNWdZNkxoMmhKY2Y4Q21OcThwTVc3RQ=="
            )
            try {
                httpURLConnection.doOutput = true
                httpURLConnection.setChunkedStreamingMode(0)
//                POST BODY DATA
                val outputStream = BufferedOutputStream(httpURLConnection.outputStream)
                val outputStreamWriter = OutputStreamWriter(outputStream)
                outputStreamWriter.write("grant_type=client_credentials")
                outputStreamWriter.flush()
                outputStreamWriter.close()
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpURLConnection.disconnect()
            }

            return null
        }
    }

    class MyGetHttpRequestTask(
        val tweetList: MutableLiveData<ArrayList<Tweet>>,
        var nextQuery: MutableLiveData<String>
    ) : AsyncTask<String, Int, String>() {

        override fun doInBackground(vararg params: String): String? {
            val searchQuery = params[0]
            val url = URL("$TWITTER_URL$searchQuery")
            val httpURLConnection = url.openConnection() as HttpURLConnection
            // setting the  Request Method Type
            httpURLConnection.requestMethod = "GET"
            // adding the headers for request
            httpURLConnection.setRequestProperty(
                "authorization",
                "Bearer AAAAAAAAAAAAAAAAAAAAADnF%2FAAAAAAAB4WKHqoMl3lCXBbMRSm5cLxguYQ%3DXbWblZL6tDAa9hhCAg8s45NZ2Xu77P27CMfIRzlnR3o07i5w1B"
            )
            try {
                Log.d("NetworkLibrary", "MyHttpRequestTask : " + httpURLConnection.responseCode)
                val bufferedReader = BufferedReader(InputStreamReader(httpURLConnection.inputStream))
                var inputLine = bufferedReader.readLine()
                val response = StringBuffer()
                while (inputLine != null) {
                    response.append(inputLine)
                    inputLine = bufferedReader.readLine()
                }
                bufferedReader.close()
                println(response.toString())

                val result = JSONObject(response.toString()).getJSONArray("statuses")
                val itemArray = ArrayList<Tweet>()
                if (result.length() < 1) {
                    itemArray.add(
                        Tweet(
                            "",
                            "",
                            "",
                            Date().toString(),
                            "No Data Found Please, try  with different search"
                        )
                    )
                    tweetList.postValue(itemArray)
                    return null
                }
                nextQuery.postValue(JSONObject(response.toString()).getJSONObject("search_metadata").getString("next_results"))

                val utcFormat = SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy", Locale.ENGLISH)
                utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"))
                val indianFormat = SimpleDateFormat("E MMM dd hh:mm:ss", Locale.ENGLISH)
                utcFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"))
                for (i in 0 until result.length()) {
                    val item = result.get(i) as JSONObject
                    val timeString = item.getString("created_at")
                    val timestamp = utcFormat.parse(timeString)
                    val output = indianFormat.format(timestamp)

                    itemArray.add(
                        Tweet(
                            item.getString("id_str"),
                            item.getJSONObject("user").getString("name"),
                            item.getJSONObject("user").getString("screen_name"),
                            output,
                            item.getString("text")
                        )
                    )
                }
                tweetList.postValue(itemArray)


            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                // this is done so that there are no open connections left when this task is going to complete
                httpURLConnection.disconnect()
            }


            return null
        }
    }


}